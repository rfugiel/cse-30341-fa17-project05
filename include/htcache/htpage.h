// htpage.h: Hash Tree Page

#pragma once

#include <cstdlib>
#include <mutex>
#include <queue>
#include <random>
#include <stdexcept>
#include <vector>

#include <ctime>
#include <chrono>

enum EvictionPolicy {
    EVICT_FIFO,
    EVICT_RANDOM,
    EVICT_LRU,
    EVICT_CLOCK,
};

template <typename KeyType, typename ValueType>
struct HTPageEntry {
    KeyType	Key;
    ValueType	Value;

    // TODO: Add bookkeeping for eviction
    size_t t_created;
    size_t t_last_accessed;
    bool clock_remove;
    bool empty;
};

template <typename KeyType, typename ValueType>
class HTPage {
    typedef std::vector<HTPageEntry<KeyType, ValueType>> EntriesType;

private:
    EntriesType		Entries;    // Individual entries
    EvictionPolicy	Policy;	    // Eviction policy
    mutable std::mutex	Lock;	    // Lock

    // TODO: Add bookkeeping for eviction

    size_t evict_fifo(size_t offset) {
        //puts("!!! - evict fifo");
    	// TODO: Implement FIFO eviction policy
        size_t t_first = Entries[offset].t_created;
        for (size_t i = 0; i < Entries.size(); i++) {
            //run through and determine which one was first created
            size_t j = (offset + i) % Entries.size();
            if (Entries[j].t_created < t_first) {
                t_first = Entries[j].t_created;
                offset = j;
            }
        }

    	return offset;
    }

    size_t evict_random(size_t offset) {
    	// TODO: Implement random eviction policy
        srand(time(NULL));

        size_t random = rand() % Entries.size();
        //Make sure it isn't pointing to offset value
        while (random == offset) {
            random = (rand() % Entries.size());
        }

    	return offset;
    }

    size_t evict_lru(size_t offset) {
    	// TODO: Implement LRU eviction policy
        //Same as FIFO except use time last accessed instead of time created
        size_t last = Entries[offset].t_last_accessed;
        for (size_t i = 0; i < Entries.size(); i++) {
            size_t j = (offset + i) % Entries.size();
            if (Entries[j].t_last_accessed < last) {
                last = Entries[j].last_used;
                offset = j;
            }
        }

    	return offset;
    }

    size_t evict_clock(size_t offset) {
    	// TODO: Implement clock eviction policy
        for (size_t i = offset; i < Entries.size(); i++) {
            size_t j = (offset + i) % Entries.size();
            if (Entries[j].clock_remove) {
                offset = j;
                break;
            }
            else {
                Entries[j].clock_remove = true;
            }
        }

        //To do...what if nothing can be removed?

    	return offset;
    }

public:
    HTPage(size_t n, EvictionPolicy p) {
        //puts("!!! - Initialize Page");
    	// TODO: Initialize Entries
        Policy = p;

        for (size_t i = 0; i < n; i++) {
            HTPageEntry<KeyType, ValueType> e;
            e.empty = true;
            e.clock_remove = false;
            Entries.push_back(e);
        }
    }

    HTPage(const HTPage<KeyType, ValueType>& other) {
        //puts("!!! - Initialize Page Copy");
    	// TODO: Copy instance variables
        std::lock_guard<std::mutex> guard1(Lock);
        std::lock_guard<std::mutex> guard2(other.Lock);
        Policy = other.Policy;
        Entries = other.Entries;

    }

    ValueType get(const KeyType &key, size_t offset) {
        //puts("!!! - get");
    	// TODO: Use linear probing to locate key
        std::lock_guard<std::mutex> guard(Lock);
        ValueType v;
        bool found = false;
        for (size_t i = 0; i < Entries.size(); i++) {
            size_t j = (offset + i) % Entries.size();
            if (Entries[j].empty) {
                //Nothing to be found
                break;
            }
            else if (!Entries[j].empty && Entries[j].Key == key) {
                auto t_now = std::chrono::system_clock::now();
                std::time_t now = std::chrono::system_clock::to_time_t(t_now);
                v = Entries[j].Value;
                Entries[j].t_last_accessed = now;
                found = true;
                break;
            }
        }

        if (!found)
            throw std::out_of_range("Not found in cache");

        return v;
    

    }

    void put(const KeyType &key, const ValueType &value, size_t offset) {
        //puts("!!! - put");
        std::lock_guard<std::mutex> guard(Lock);
    	// TODO: Use linear probing to locate key
        bool found = false;
        for (size_t i = 0; i < Entries.size(); ++i) {
            size_t j = (offset + i) % Entries.size();
            if (Entries[j].empty) {
                auto t_now = std::chrono::system_clock::now();
                std::time_t now = std::chrono::system_clock::to_time_t(t_now);

                found = true;
                Entries[j].Key = key;
                Entries[j].t_created = now;
                Entries[j].t_last_accessed = now;
                offset = j;
                Entries[j].empty = false;
                break;
            }
            else if (Entries[j].Key == key) {
                    auto t_now = std::chrono::system_clock::now();
                    std::time_t now = std::chrono::system_clock::to_time_t(t_now); 
                    found = true;
                    Entries[j].t_last_accessed = now;
                    Entries[j].empty = false;
                    offset = j;
                    break;
            }
            
        }

        // TODO: Evict an entry if HTPage is full
        if (!found) {
            if (Policy == EVICT_FIFO){
                offset = evict_fifo(offset);
            }
            else if (Policy == EVICT_RANDOM){
                offset = evict_random(offset);
            }
            else if (Policy == EVICT_LRU){
                offset = evict_random(offset);
            }
            else if (Policy == EVICT_CLOCK){
                offset = evict_clock(offset);
            }

            auto t_now = std::chrono::system_clock::now();
            std::time_t now = std::chrono::system_clock::to_time_t(t_now); 

            Entries[offset].Key = key;
            Entries[offset].t_created = now;
            Entries[offset].t_last_accessed = now;
        }

        // TODO: Update entry
        Entries[offset].clock_remove = false;
        Entries[offset].Value = value;



    }
};
