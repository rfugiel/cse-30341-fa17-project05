// htcache.h: Hash Tree Cache

#pragma once

#include <htcache/htpage.h>

#include <cstdio>
#include <cstdlib>
#include <vector>
#include <mutex> 

#include <math.h>

template <typename KeyType, typename ValueType>
class HTCache {
    typedef ValueType (*HandlerFunc)(const KeyType&);
    typedef std::vector<HTPage<KeyType, ValueType>> TableType;

private:
    size_t	    AddressLength;  // Length of virtual address
    size_t	    PageSize;	    // Number of entries per page
    EvictionPolicy  Policy;	    // Eviction policy
    HandlerFunc	    Handler;	    // Handler for misses
    TableType	    PageTable;	    // Page table

    size_t	    Addresses;	    // Number of addresses
    size_t	    Pages;	    // Number of pages
    size_t	    VPNShift;	    // VPN Bit Shift
    size_t	    VPNMask;	    // VPN Bit Mask
    size_t	    OffsetMask;	    // Offset Bit Mask

    size_t	    Hits = 0;	    // Number of cache hits
    size_t	    Misses = 0;	    // Number of cache misses

    std::mutex	    Lock;	    // Lock

public:
    HTCache(size_t addrlen, size_t pagesize, EvictionPolicy policy, HandlerFunc handler)
            : AddressLength(addrlen), PageSize(pagesize), Policy(policy), Handler(handler)  {
        // ^^^ Initialize values

    	if (addrlen == 0) {
            Addresses = 0;
            Pages = 0;
            VPNShift = 0;
            VPNMask = 0;
            OffsetMask = 0;
    	    return;
	    }

        //puts("!!! - Initializing Cache");
        // TODO: Determine Addresses, Pages, VPNShift, VPNMask, OffsetMask        
        Addresses = pow(2, AddressLength);
        Pages = Addresses / PageSize;
        VPNShift = log2(PageSize);  

        // Use highest possible address to get masks
        size_t max_addr = (1 << addrlen) - 1;
        VPNMask = ((max_addr >> VPNShift ) << (size_t)log2(PageSize) );
        OffsetMask = (max_addr >> (AddressLength - (size_t)log2(PageSize)));

        // TODO: Initialize PageTable
        for (size_t i = 0; i < Pages; i++) {
            PageTable.push_back(HTPage<KeyType, ValueType>(PageSize, policy));
        }
    
    }

    ValueType	get(const KeyType &key) {
        //puts("!!! - cache get");
        if (PageTable.empty()) {
                //puts("!!! - empty....runnign handler");
        	   return Handler(key);
    	}

    	// TODO: Determine virtual address, VPN, offset
        // Use hash to get address
        size_t virtual_address = std::hash<KeyType>()(key) % Addresses;

        //apply masks that we initialized in constructor
        size_t VPN = (virtual_address & VPNMask) >> VPNShift;
        size_t offset = virtual_address & OffsetMask;

    	// TODO: Retrieve value from HTPage
    	ValueType value;
            try {
                // If found....
                value = PageTable[VPN].get(key, offset);
                Lock.lock();
                Hits++;
                Lock.unlock();
            }
            catch (std::out_of_range &oor) {
                //If not found... we need to use handler
                Lock.lock();
                Misses++;
                Lock.unlock();
                value = Handler(key);
                put(key, value);
            }
        	return value;
    }

    void	put(const KeyType &key, const ValueType &value) {
    	if (PageTable.empty()) {
    	    return;
	}

	// TODO: Determine virtual address, VPN, offset
    size_t virtual_address = std::hash<KeyType>()(key) % Addresses;
    size_t VPN = (virtual_address & VPNMask) >> VPNShift;
    size_t offset = virtual_address & OffsetMask;


	// TODO: Set key, value in HTPage
    PageTable[VPN].put(key, value, offset);
    }

    void stats(FILE *stream) {
	fprintf(stream, "Addresses : %lu\n"  , Addresses);
	fprintf(stream, "Pages     : %lu\n"  , Pages);
	fprintf(stream, "VPNShift  : %lu\n"  , VPNShift);
	fprintf(stream, "VPNMask   : 0x%lX\n", VPNMask);
	fprintf(stream, "OffsetMask: 0x%lX\n", OffsetMask);
    	fprintf(stream, "Hits      : %lu\n"  , Hits);
    	fprintf(stream, "Misses    : %lu\n"  , Misses);
    }
};
