CSE.30341.FA17: Project 05
==========================

This is the documentation for [Project 05] of [CSE.30341.FA17].

Members
-------

1. Radomir Fugiel (rfugiel@nd.edu)

Design
------

> 1. In the `HTCache` constructor, you will need to determine the following
>    values: `Addresses`, `Pages`, `VPNShift`, `VPNMask`, and `OffsetMask`.
>    How would you determine these values and what would they be used for?
Adresses = 2 to the power of addrlen
Pages = Adresses/Pagesize
VPN = log2(page size)
VPN and Offet mask are using the bit mask and shift operation



> 2. In the `HTCache::get` method, you will need to determine the **VPN** and
>    **offset** based on the specified **key**.
> 
>       - How would you determine these values?
>       - How would you use these values to retrieve the specified value?
>       - What data would you need to update?
>       - What data would you need to synchronize?

You would use the VPN mask and OffsetMask in conjuctionw with the VPN shift.
I would use the VPN to get the HTPage and the Offset to get an entry. You would need to update the entry if it dows not exist when you get it. We would need to sync this whenever we get


> 3. In the `HTCache::put` method, you will need to determine the **VPN** and
>    **offset** based on the specified **key**.
> 
>       - How would you determine these values?
>       - How would you use these values to store the specified value?
>       - What data would you need to update?
>       - What data would you need to synchronize?

Same as above
I would updaye whatever value is being pointed to by the key, to resolve any collisions linear probing will be used. I would sync any values changed


> 3. In the `HTPage` class, you will need to implement FIFO, Random, LRU, and
>    Clock eviction policies.  For each of these policies:
> 
>       - What bookkeeping do you need to implement the policy?
>       - How would you use this bookkeeping to implement the policy?

Fifo: -record time of creation and use that to figure out which to evict

Random: It's random... so just randomly.

LRU: keep track of whenver entry is accessed

Clock: Basically keep a "used bit" whenever accessing 

> 5. In the `HTPage::get`, you must use linear probing to locate an appropriate
>    `HTPageEntry`.
> 
>       - How would you determine if you found the correct `HTPageEntry`?

I would keep moving down the list of entires until I found one that matches what I am lookign for

> 6. In the `HTPage::put`, you must use linear probing to locate an appropriate
>    `HTPageEntry`.
> 
>       - How would you determine if you need to update a value rather than add a new one?
>       - How would you determine if you need to perform an eviction?
>       - How would you perform an eviction?

Check if it is already filled then if so I would update otherwise add, if all filled then I need to evict which I would do by replacing the entry I am evicting with the new entry

Demonstration
-------------

> Place a link to your demonstration slides on [Google Drive].

https://docs.google.com/presentation/d/1bZwfuA-ujMu9OMF3CleJ7wxSfOYpQD-vxCvak8u5DSI/edit?usp=sharing

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 05]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project05.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Drive]:     https://drive.google.com
