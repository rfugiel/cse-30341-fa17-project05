// fibonacci.cpp

#include <htcache/htcache.h>

#include <cstdio>
#include <thread>

#include <iostream>
#include <iomanip>

// Cache used in computing sequence
HTCache<uint64_t, uint64_t> *fib_cache;

//output formatted results
void fib_thread(HTCache<uint64_t, uint64_t> *cache, size_t count) {
    // Go up to 100
    for (uint64_t i = 0; i < 100; i++) {
        std::cout << " " << count << ". Fibonacci(" << std::setw(2) << i << ") = " << cache->get(i) << std::endl;
    }
}

//return next value in pattern
uint64_t fib_handler(const uint64_t &key) {
    if (key == 0 || key == 1) {
        //Base values
        return key;
    }else{
    	return fib_cache->get(key - 1) + fib_cache->get(key - 2);
	}
}

    
int main(int argc, char *argv[]) {
    if (argc != 5) {
    	fprintf(stderr, "Usage: %s AddressLength PageSize EvictionPolicy Threads\n", argv[0]);
    	return EXIT_FAILURE;
    }

    size_t addrlen 		  = strtol(argv[1], NULL, 10);
    size_t pagesize 	  = strtol(argv[2], NULL, 10);
    EvictionPolicy policy = static_cast<EvictionPolicy>(strtol(argv[3], NULL, 10));
    size_t nthreads       = strtol(argv[4], NULL, 10);

    HTCache<uint64_t, uint64_t> cache(addrlen, pagesize, policy, fib_handler);
    //Use global cache that handler can access
    fib_cache = &cache;    

    std::thread t[nthreads];
    for (size_t i = 0; i < nthreads; i++) {
    	t[i] = std::thread(fib_thread, fib_cache, i);
    }

    for (size_t i = 0; i < nthreads; i++) {
    	t[i].join();
    }

    fib_cache->stats(stdout);
    fflush(stdout);
    return EXIT_SUCCESS;
}